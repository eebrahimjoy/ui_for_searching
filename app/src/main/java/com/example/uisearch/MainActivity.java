package com.example.uisearch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.uisearch.databinding.ActivitySearchBinding;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ActivitySearchBinding binding;
    SearchCatAdapter adapter;
    RecentSearchedCatAdapter adapter2;

    private List<String> serviceType = new ArrayList<>();
    private List<String> recentSearched = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_search);

        serviceType.add("Bail and Application");
        serviceType.add("Divorce and fight");
        serviceType.add("Interpretation for fight");
        serviceType.add("Murder and others");

        recentSearched.add("Bail and Application");
        recentSearched.add("Divorce and fight");
        recentSearched.add("Murder and others quarrel");
        recentSearched.add("Interpretation for fight");
        recentSearched.add("Bail and Application");
        recentSearched.add("Divorce and fight");
        recentSearched.add("Murder and others quarrel");
        recentSearched.add("Interpretation for fight");
        recentSearched.add("Bail and Application");
        recentSearched.add("Divorce and fight");
        recentSearched.add("Murder and others quarrel");
        recentSearched.add("Interpretation for fight");
        recentSearched.add("Bail and Application");
        recentSearched.add("Divorce and fight");
        recentSearched.add("Murder and others quarrel");
        recentSearched.add("Interpretation for fight");
        initRecyclerView();
        initRecyclerView2();
        hideSearchViewIcon();

    }
    private void initRecyclerView() {

        adapter = new SearchCatAdapter(serviceType,this);
        binding.cancelRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        binding.cancelRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();


    }
    private void initRecyclerView2() {
        adapter2 = new RecentSearchedCatAdapter(recentSearched,this);
        binding.recentSearchedRV.setLayoutManager(new LinearLayoutManager(this));
        binding.recentSearchedRV.setAdapter(adapter2);
    }
    private void hideSearchViewIcon() {
        int magId = getResources().getIdentifier("android:id/search_mag_icon", null, null);
        ImageView magImage = (ImageView) binding.searchSV.findViewById(magId);
        magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
        magImage.setVisibility(View.GONE);
    }
}
