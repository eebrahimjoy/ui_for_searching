package com.example.uisearch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.uisearch.databinding.ModelSearchTypeItemBinding;
import com.example.uisearch.databinding.ModlRecentSearchBinding;

import java.util.List;


class RecentSearchedCatAdapter extends RecyclerView.Adapter<RecentSearchedCatAdapter.ViewHolder> {

    private List<String> cancelIssueList;
    private Context context;


    public RecentSearchedCatAdapter(List<String> cancelIssueList, Context context) {
        this.cancelIssueList = cancelIssueList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecentSearchedCatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ModlRecentSearchBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.modl_recent_search, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecentSearchedCatAdapter.ViewHolder viewHolder, final int i) {
        final String type = cancelIssueList.get(i);
        viewHolder.binding.typeNameTV.setText(type);

    }

    @Override
    public int getItemCount() {
        return cancelIssueList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ModlRecentSearchBinding binding;

        public ViewHolder(ModlRecentSearchBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
