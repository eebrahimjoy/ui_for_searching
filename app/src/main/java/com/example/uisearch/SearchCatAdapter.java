package com.example.uisearch;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.uisearch.databinding.ModelSearchTypeItemBinding;

import java.util.List;


class SearchCatAdapter extends RecyclerView.Adapter<SearchCatAdapter.ViewHolder> {

    private List<String> cancelIssueList;
    private Context context;


    public SearchCatAdapter(List<String> cancelIssueList, Context context) {
        this.cancelIssueList = cancelIssueList;
        this.context = context;
    }

    @NonNull
    @Override
    public SearchCatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ModelSearchTypeItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.model_search_type_item, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final SearchCatAdapter.ViewHolder viewHolder, final int i) {
        final String type = cancelIssueList.get(i);
        viewHolder.binding.cancelComplementTV.setText(type);

    }

    @Override
    public int getItemCount() {
        return cancelIssueList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ModelSearchTypeItemBinding binding;

        public ViewHolder(ModelSearchTypeItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
